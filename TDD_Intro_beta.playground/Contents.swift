import Foundation
import PlaygroundSupport
import XCTest

class FizzBuzzTests: XCTestCase
{
    override func setUp()
    {
        
    }

    override func tearDown()
    {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testShouldFail()
    {
        XCTFail("You must fail to succeed!")
    }
    
    func testShouldPass()
    {
        XCTAssertEqual(2+2, 4, "2+2 should be 4")
    }
}


TestRunner().runTests(testClass: FizzBuzzTests.self)





/* ------------------------------------------------------ */

class PlaygroundTestObserver: NSObject, XCTestObservation
{
    @objc func testCase(_ testCase: XCTestCase, didFailWithDescription description: String, inFile filePath: String?, atLine lineNumber: Int) {
        print("Test failed on line \(lineNumber): \(testCase.name), \(description)")
    }
}

let observer = PlaygroundTestObserver()
let center = XCTestObservationCenter.shared
center.addTestObserver(observer)


